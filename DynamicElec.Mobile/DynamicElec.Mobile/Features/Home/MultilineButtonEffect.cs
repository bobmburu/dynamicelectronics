﻿using Xamarin.Forms;

namespace DynamicElec.Mobile.Features.Home
{
    public class MultilineButtonEffect : RoutingEffect
    {
        public MultilineButtonEffect()
            : base($"{nameof(DynamicElec)}.{nameof(MultilineButtonEffect)}")
        {
        }
    }
}
