﻿using System.Collections.Generic;
using DynamicElec.Mobile.Features.Product;

namespace DynamicElec.Mobile.Features.Home
{
    public class LandingDTO
    {
        public IEnumerable<ProductDTO> PopularProducts { get; set; }
    }
}