﻿using System.Threading.Tasks;
using Refit;
using DynamicElec.Mobile.Features.Common;
using DynamicElec.Mobile.Features.Product;

namespace DynamicElec.Mobile.Features.Home
{
    public class FakeHomeAPI : IHomeAPI
    {
        public Task<LandingDTO> GetAsync([Header(Settings.Settings.ApiAuthorizationHeader)] string authorizationHeader)
            => FakeNetwork.ReturnAsync(new LandingDTO { PopularProducts = FakeProducts.Fakes, });
    }
}
