﻿using System.Collections.Generic;
using Newtonsoft.Json;
using DynamicElec.Mobile.Features.Common;

namespace DynamicElec.Mobile.Features.Product
{
    public static class FakeProducts
    {
        public static IEnumerable<ProductDTO> Fakes => 
            JsonConvert.DeserializeObject<IEnumerable<ProductDTO>>(
                EmbeddedResourceHelper.Load("DynamicElec.Mobile.Features.Product.FakeProducts.json"));
    }
}
