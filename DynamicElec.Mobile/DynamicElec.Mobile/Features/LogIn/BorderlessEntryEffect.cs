﻿using Xamarin.Forms;

namespace DynamicElec.Mobile.Features.LogIn
{
    public class BorderlessEntryEffect : RoutingEffect
    {
        public BorderlessEntryEffect()
            : base($"{nameof(DynamicElec)}.{nameof(BorderlessEntryEffect)}")
        {
        }
    }
}
