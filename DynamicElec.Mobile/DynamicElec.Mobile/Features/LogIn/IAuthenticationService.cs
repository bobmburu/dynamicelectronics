﻿using System.Threading.Tasks;

namespace DynamicElec.Mobile.Features.LogIn
{
    public interface IAuthenticationService
    {
        string AuthorizationHeader { get; }

        bool IsAnyOneLoggedIn { get; }

        Task LogInAsync(string email, string password);
    }
}