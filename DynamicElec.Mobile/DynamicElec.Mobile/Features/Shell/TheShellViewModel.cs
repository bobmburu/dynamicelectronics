using System.Windows.Input;
using DynamicElec.Mobile.Features.Common;
using DynamicElec.Mobile.Features.LogIn;
using DynamicElec.Mobile.Features.Product.Category;
using DynamicElec.Mobile.Features.Scanning.Photo;

namespace DynamicElec.Mobile.Features.Shell
{
    internal class TheShellViewModel : BaseViewModel
    {
        public ICommand PhotoCommand => new AsyncCommand(
            _ => App.NavigateModallyToAsync(new CameraPage(), animated: false));

        public ICommand ARCommand => FeatureNotAvailableCommand;

        public ICommand LogOutCommand => new AsyncCommand(_ => App.NavigateModallyToAsync(new LogInPage()));

        public ICommand ProductTypeCommand => new AsyncCommand(
            typeId => App.NavigateToAsync(new ProductCategoryPage(typeId as string), closeFlyout: true));

        public ICommand ProfileCommand => FeatureNotAvailableCommand;
    }
}