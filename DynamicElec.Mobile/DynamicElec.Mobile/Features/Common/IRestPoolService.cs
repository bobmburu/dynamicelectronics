﻿using System;
using DynamicElec.Mobile.Features.Home;
using DynamicElec.Mobile.Features.LogIn;
using DynamicElec.Mobile.Features.Product;

namespace DynamicElec.Mobile.Features.Common
{
    public interface IRestPoolService
    {
        Lazy<IProfilesAPI> ProfilesAPI { get; }

        Lazy<IHomeAPI> HomeAPI { get; }

        Lazy<IProductsAPI> ProductsAPI { get; }
    }
}