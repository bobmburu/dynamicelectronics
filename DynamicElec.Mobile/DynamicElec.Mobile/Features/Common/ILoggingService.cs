﻿using System;

namespace DynamicElec.Mobile.Features.Common
{
    public interface ILoggingService
    {
        void Debug(string message);

        void Warning(string message);

        void Error(Exception exception);
    }
}