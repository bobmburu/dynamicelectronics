﻿using System;
using Refit;
using DynamicElec.Mobile.Features.Home;
using DynamicElec.Mobile.Features.LogIn;
using DynamicElec.Mobile.Features.Product;

namespace DynamicElec.Mobile.Features.Common
{
    public class RestPoolService : IRestPoolService
    {
        public Lazy<IProfilesAPI> ProfilesAPI => new Lazy<IProfilesAPI>(
            () => RestService.For<IProfilesAPI>(HttpClientFactory.Create(Settings.Settings.ProfilesApiUrl)));

        public Lazy<IHomeAPI> HomeAPI => new Lazy<IHomeAPI>(
            () => RestService.For<IHomeAPI>(HttpClientFactory.Create(Settings.Settings.ProductApiUrl)));

        public Lazy<IProductsAPI> ProductsAPI => new Lazy<IProductsAPI>(
            () => RestService.For<IProductsAPI>(HttpClientFactory.Create(Settings.Settings.ProductApiUrl)));
    }
}
