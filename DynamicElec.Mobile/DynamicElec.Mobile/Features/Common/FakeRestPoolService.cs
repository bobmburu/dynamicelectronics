﻿using System;
using DynamicElec.Mobile.Features.Home;
using DynamicElec.Mobile.Features.LogIn;
using DynamicElec.Mobile.Features.Product;

namespace DynamicElec.Mobile.Features.Common
{
    public class FakeRestPoolService : IRestPoolService
    {
        public Lazy<IProfilesAPI> ProfilesAPI => new Lazy<IProfilesAPI>(() => new FakeProfilesAPI());

        public Lazy<IHomeAPI> HomeAPI => new Lazy<IHomeAPI>(() => new FakeHomeAPI());

        public Lazy<IProductsAPI> ProductsAPI => new Lazy<IProductsAPI>(() => new FakeProductsAPI());
    }
}
