﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DynamicElec.Mobile.Features.Product;

namespace DynamicElec.Mobile.Features.Scanning.Photo
{
    public interface IVisionService
    {
        Task<IEnumerable<ProductDTO>> GetRecommendedProductsFromPhotoAsync(string photoPath);
    }
}
