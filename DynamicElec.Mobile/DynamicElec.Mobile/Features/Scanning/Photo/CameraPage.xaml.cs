﻿using Xamarin.Forms;

namespace DynamicElec.Mobile.Features.Scanning.Photo
{
    public partial class CameraPage
    {
        public CameraPage()
        {
            InitializeComponent();

            BindingContext = new CameraViewModel();
        }
    }
}
