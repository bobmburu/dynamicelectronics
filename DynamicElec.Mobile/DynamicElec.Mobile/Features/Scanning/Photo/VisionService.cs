﻿using Refit;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using DynamicElec.Mobile.Features.Common;
using DynamicElec.Mobile.Features.LogIn;
using DynamicElec.Mobile.Features.Product;
using DynamicElec.Mobile.Features.Scanning.Photo;
using Xamarin.Forms;

[assembly: Dependency(typeof(VisionService))]

namespace DynamicElec.Mobile.Features.Scanning.Photo
{
    public class VisionService : IVisionService
    {
        private readonly IAuthenticationService authenticationService;
        private readonly IProductsAPI productsAPI;

        public VisionService()
        {
            authenticationService = DependencyService.Get<IAuthenticationService>();
            productsAPI = DependencyService.Get<IRestPoolService>().ProductsAPI.Value;
        }

        public async Task<IEnumerable<ProductDTO>> GetRecommendedProductsFromPhotoAsync(string photoPath)
        {
            using (var photoStream = File.Open(photoPath, FileMode.Open))
            {
                var streamPart = new StreamPart(photoStream, "photo.jpg", "image/jpeg");

                return await productsAPI.GetSimilarProductsAsync(authenticationService.AuthorizationHeader, streamPart);
            }
        }
    }
}
